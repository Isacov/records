<?php

use App\Http\Controllers\Api\RecordsController;
use Illuminate\Support\Facades\Route;


Route::get('records/by-agent', [RecordsController::class, 'byAgent']);

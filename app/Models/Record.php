<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Record extends Model
{
    use HasFactory;

    protected $fillable = [
        'agent', 'from', 'disposition', 'duration', 'queue', 'to', 'timestamp',
        'uuid', 'wrapup_code', 'type', 'agent_id', 'cpc', 'recordingfile'
    ];

    protected $casts = [
        'timestamp' => 'datetime',
    ];
}

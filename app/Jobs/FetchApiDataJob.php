<?php

namespace App\Jobs;

use App\Services\OmniService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class FetchApiDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public int $tries = 5;

    protected OmniService $omniService;

    public function __construct(OmniService $omniService)
    {
        $this->omniService = $omniService;
    }

    public function handle()
    {
        try {
            $batchSize = 1000;

            for ($page = 1; $page <= 100; $page++) {
                $data = $this->omniService->getRecords($page);

                if ($data && isset($data['records'])) {
                    $chunks = array_chunk($data['records'], $batchSize);

                    foreach ($chunks as $chunk) {
                        DB::table('records')->insert($chunk);
                    }
                }

                gc_collect_cycles();
            }

            Log::info('API data fetched and stored successfully');
        } catch (\Exception $e) {
            Log::error('Unhandled exception in FetchApiDataJob: ' . $e->getMessage());
        }
    }

    public function failed(\Exception $e)
    {
        Log::error('Job failed: ' . $e->getMessage());
    }
}

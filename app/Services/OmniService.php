<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

class OmniService
{
    protected string $baseUrl;

    public function __construct()
    {
        $this->baseUrl = 'https://omni.salox.tech/api/v1/fdb659ec6744d718610e47647efbbd7bce77fe025e443dev';
    }

    public function getRecords(int $page)
    {
        try {
            $response = Http::get("{$this->baseUrl}/cdr?page={$page}");

            if ($response->successful()) {
                return $response->json();
            } else {
                Log::error("Failed to fetch data from page {$page}");
                return null;
            }
        } catch (\Exception $e) {
            Log::error("HTTP request failed for page {$page}: " . $e->getMessage());
            return null;
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class RecordsController extends Controller
{
    public function byAgent()
    {
        $stats = DB::table('records')
            ->select(
                'agent',
                DB::raw('COUNT(*) as total_calls'),
                DB::raw('SUM(CASE WHEN recordingfile IS NOT NULL AND recordingfile != "" THEN 1 ELSE 0 END) as calls_with_recording'),
                DB::raw('SUM(CASE WHEN recordingfile IS NULL OR recordingfile = "" THEN 1 ELSE 0 END) as calls_without_recording'),
                DB::raw('SEC_TO_TIME(SUM(TIME_TO_SEC(duration))) as total_duration')
            )
            ->groupBy('agent')
            ->get();

        return response()->json($stats);
    }
}

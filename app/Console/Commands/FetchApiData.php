<?php

namespace App\Console\Commands;

use App\Jobs\FetchApiDataJob;
use App\Services\OmniService;
use Illuminate\Console\Command;

class FetchApiData extends Command
{
    protected $signature = 'fetch:api-data';
    protected $description = 'Fetch data from API and store in the database';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        FetchApiDataJob::dispatch(new OmniService());
        $this->info('API data fetching job dispatched successfully');
    }
}

## Record api prerequisites:
- **[Docker](https://docs.docker.com/engine/install/) and [Docker Compose](https://docs.docker.com/compose/install/)**

## Project installation:
### 1. Download from git
``cd directory``
### 2. Go to folder
``cd directory``
### 3. Run docker
``docker-compose up`` 
### 4. Open new terminal
``docker-compose exec php php artisan migrate`` - to add tables in db

``docker-compose exec php php artisan queue:work`` - to run queue

``docker-compose exec php php artisan fetch:api-data`` - to insert data into db

### 5. Open browser or use postman.
``http://localhost/api/records/by-agent``



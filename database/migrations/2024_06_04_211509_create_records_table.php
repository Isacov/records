<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('records', function (Blueprint $table) {
            $table->id();
            $table->string('agent')->nullable();
            $table->string('from')->nullable();
            $table->string('disposition');
            $table->time('duration');
            $table->string('queue')->nullable();
            $table->string('to')->nullable();
            $table->timestamp('timestamp');
            $table->string('uuid');
            $table->string('wrapup_code')->nullable();
            $table->string('type')->nullable();
            $table->string('agent_id')->nullable();
            $table->string('cpc')->nullable();
            $table->string('recordingfile')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('records');
    }
};
